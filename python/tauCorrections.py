import os
import envyaml

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/TAU/python/tauCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class TauSFRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year");  year = str(self.year)
        self.vsjet_wps = kwargs.pop("vsjet_wps", ["Medium", "Tight"])
        self.vse_wps = kwargs.pop("vse_wps", ["VVLoose", "Tight"])
        self.vsmu_wps = kwargs.pop("vsmu_wps", ["VLoose", "Tight"])
        self.vsjet_vse_wp = kwargs.pop("vsjet_vse_wp", "VVLoose")

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass
        
        self.corrKey = prefix+year

        if self.isMC:
            if not isUL and self.year <= 2018:
                raise ValueError("Only implemented for Run2 UL datasets")

            if not os.getenv("_TauSF"):
                os.environ["_TauSF"] = "_TauSF"

                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libBaseModules.so")
                ROOT.gInterpreter.Declare(os.path.expandvars(
                    '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_vsjet = MyCorrections("%s", "%s");' % 
                        (corrCfg[self.corrKey]["fileName"], corrCfg[self.corrKey]["corrName"]+"VSjet"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_vse = MyCorrections("%s", "%s");' % 
                        (corrCfg[self.corrKey]["fileName"], corrCfg[self.corrKey]["corrName"]+"VSe"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_vsmu = MyCorrections("%s", "%s");' % 
                        (corrCfg[self.corrKey]["fileName"], corrCfg[self.corrKey]["corrName"]+"VSmu"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_tes = MyCorrections("%s", "%s");' % 
                        (corrCfg[self.corrKey]["fileName"], corrCfg[self.corrKey]["corrNameTES"]))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<double> get_deeptau_vsjet_sf(
                            Vfloat pt, Vint dm, Vint genmatch,
                            std::string wp, std::string wp_vse, std::string syst, std::string flag) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (flag == "dm" && (pt[i] <= 40 || (dm[i] > 2 && dm[i] < 10))) {
                                sf.push_back(1.);
                            }
                            else
                                sf.push_back(corr_vsjet.eval({pt[i], dm[i], genmatch[i], wp, wp_vse, syst, flag}));
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_deeptau_vse_sf(
                            Vfloat eta, Vint dm, Vint genmatch, std::string wp, 
                            std::string syst, std::string idV) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < eta.size(); i++) {
                            if (dm[i] >= 2 && dm[i] < 10) sf.push_back(1.);
                            else if (idV == "DeepTau2017v2p1") {
                                sf.push_back(corr_vse.eval({eta[i], genmatch[i], wp, syst}));
                            }
                            else { 
                                sf.push_back(corr_vse.eval({eta[i], dm[i], genmatch[i], wp, syst}));
                            }
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_deeptau_vsmu_sf(
                            Vfloat eta, Vint genmatch, std::string wp, std::string syst) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < eta.size(); i++) {
                            sf.push_back(corr_vsmu.eval({eta[i], genmatch[i], wp, syst}));
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_tes(
                                Vfloat pt, Vfloat eta, Vint dm, Vint genmatch, std::string idV, 
                                std::string wp, std::string wp_vse, std::string syst) {
                            ROOT::RVec<double> tes_factor;
                            for (size_t i = 0; i < eta.size(); i++) {
                                if (dm[i] == 5 || dm[i] == 6) tes_factor.push_back(1.);
                                else if (idV == "DeepTau2017v2p1") {
                                    tes_factor.push_back(corr_tes.eval({pt[i], eta[i], dm[i], genmatch[i], idV, syst}));
                                }
                                else {
                                    tes_factor.push_back(corr_tes.eval({pt[i], eta[i], dm[i], genmatch[i], idV, wp, wp_vse, syst}));
                                }
                            }
                            return tes_factor;
                        }
                """)

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        for syst_name, syst in [("", "nom"), ("_up", "up"), ("_down", "down")]:
            for wp in self.vsjet_wps:
                # pt binned SFs are available only for DeepTau2017v2p1
                if corrCfg[self.corrKey]["corrName"] == "DeepTau2017v2p1":
                    df = df.Define("Tau_sf%sVSjet_pt_binned_%s%s" %
                                    (corrCfg[self.corrKey]["corrName"], wp, syst_name),
                                    'get_deeptau_vsjet_sf(Tau_pt, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s", "pt")' %
                                    (wp, self.vsjet_vse_wp, syst))

                    branches.append("Tau_sf%sVSjet_pt_binned_%s%s" %
                                    (corrCfg[self.corrKey]["corrName"], wp, syst_name))

                df = df.Define("Tau_sf%sVSjet_dm_binned_%s%s" % 
                                (corrCfg[self.corrKey]["corrName"], wp, syst_name),
                                'get_deeptau_vsjet_sf(Tau_pt, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s", "dm")' % 
                                (wp, self.vsjet_vse_wp, syst))

                branches.append("Tau_sf%sVSjet_dm_binned_%s%s" % 
                                (corrCfg[self.corrKey]["corrName"], wp, syst_name))

                # tes factor is WP dependent for DeepTau2018v2p5
                if self.year >= 2022:
                    df = df.Define("tes_factor_%s%s" % (wp, syst_name),
                                    'get_tes(Tau_pt, Tau_eta, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s", "%s")' % 
                                    (corrCfg[self.corrKey]["corrName"], wp, self.vsjet_vse_wp, syst))
                    
                    df = df.Define("Tau_pt_corr_%s%s" % (wp, syst_name),
                                    "Tau_pt * tes_factor_%s%s" % (wp, syst_name))
                    
                    df = df.Define("Tau_mass_corr_%s%s" % (wp, syst_name),
                                    "Tau_mass * tes_factor_%s%s"  % (wp, syst_name))
                
                    branches.append("tes_factor_%s%s" % (wp, syst_name))
                    branches.append("Tau_pt_corr_%s%s" % (wp, syst_name))
                    branches.append("Tau_mass_corr_%s%s" % (wp, syst_name))

            for wp in self.vse_wps:
                df = df.Define("Tau_sf%sVSe_%s%s" % 
                                (corrCfg[self.corrKey]["corrName"], wp, syst_name),
                                'get_deeptau_vse_sf(Tau_eta, Tau_decayMode, Tau_genPartFlav, "%s", "%s", "%s")' % 
                                (wp, syst, corrCfg[self.corrKey]["corrName"]))

                branches.append("Tau_sf%sVSe_%s%s" % 
                                (corrCfg[self.corrKey]["corrName"], wp, syst_name))

            for wp in self.vsmu_wps:
                df = df.Define("Tau_sf%sVSmu_%s%s" % 
                                (corrCfg[self.corrKey]["corrName"], wp, syst_name),
                                'get_deeptau_vsmu_sf(Tau_eta, Tau_genPartFlav, "%s", "%s")' % 
                                (wp, syst))

                branches.append("Tau_sf%sVSmu_%s%s" % 
                                (corrCfg[self.corrKey]["corrName"], wp, syst_name))

            # tes factor is NOT WP dependent for DeepTau2017v2p1
            if self.year <= 2018:
                df = df.Define("tes_factor%s" % syst_name,
                                'get_tes(Tau_pt, Tau_eta, Tau_decayMode, Tau_genPartFlav, "%s", "dummy", "dummy", "%s")' % 
                                (corrCfg[self.corrKey]["corrName"], syst))
                
                df = df.Define("Tau_pt_corr%s" % syst_name,
                                "Tau_pt * tes_factor%s" % syst_name)
                
                df = df.Define("Tau_mass_corr%s" % syst_name,
                                "Tau_mass * tes_factor%s"  % syst_name)
            
                branches.append("tes_factor%s" % syst_name)
                branches.append("Tau_pt_corr%s" % syst_name)
                branches.append("Tau_mass_corr%s" % syst_name)

        return df, branches


def tauSFRDF(**kwargs):
    """
    Module to obtain DeepTau2017v2p1/DeepTau2018v2p5 SFs and TES with their up/down uncertainties.

    :param vsjet_wps: DeepTau2017v2p1VSjet/DeepTau2018v2p5VSjet WPs to consider. Default: `[Medium, Tight]`
    :type vsjet_wps: list of str

    :param vse_wps: DeepTau2017v2p1VSe/DeepTau2018v2p5VSe WPs to consider.  Default: `[VVLoose, Tight]`
    :type vse_wps: list of str

    :param vsmu_wps: DeepTau2017v2p1VSmu/DeepTau2018v2p5VSmu WPs to consider. Default: `[VLoose, Tight]`
    :type vsmu_wps: list of str

    :param vsjet_vse_wp: DeepTau2017v2p1VSeWP/DeepTau2018v2p5VSeWP used to compute the DeepTau2017v2p1VSjet/DeepTau2018v2p5VSjet scale factor.
        Default: `VVLoose`
    :type vsjet_vse_wp: str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: tauSFRDF
            path: Corrections.TAU.tauCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                isUL: self.dataset.has_tag('ul')
                runPeriod: self.dataset.runPeriod
                vsjet_wps: [Medium, Tight]
                vse_wps: [VVLoose, Tight]
                vsmu_wps: [VLoose, Tight]

    """

    return lambda: TauSFRDFProducer(**kwargs)
